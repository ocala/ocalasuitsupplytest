SuitSupply Demandware Test
==========================

This is the source code repository for my answer to the suitsupply test. 

The developed LINK cartridge was named int_bring. That name was assigned because it is the hypotetical base name to develop a full cartridge to integrate all the webservices offered by [Bring](http://developer.bring.com/api/pickup-point/#pickup-points-for-postal-code). But in this case only one service, in one API (of the 8 offered APIs) was implemented. 

The developed cartridge components are the following:

A jQuery plugin with the following responsabilities:

- Identify the postalcode field and the country field. 
- After the users types in the information, get the field values
- Call a backend side pipeline to obtain the pickup locations
- Render the pickup location within the GUI
- Render other messages for other application states.

A backend demandware pipeline with the following responsabilities:

- Obtain the parameters sent by the client side
- invoke the Bring webservice. The webservice consumption uses the Service Framework released with DW Platform 15.2. The integration is still compatible with storefronts based on older SiteGenesis releases. Even though, the integration should run on a runtime environment with DW Platform 15.2+ (Almost all the DW PODS knowadays).
- Cache the obtained results
- provide ISML files and modules to make the cartridge easy integrable.


Live example 
------------

A live can be found [link](http://ocala-suitsupply-test.javawebdev.com/electronics) 

To use the live example:
1) Go to the link 
2) Add a product to the bag 
3) Go to the checkout page 
4) Type the postal code (optionally change first the country field)

Design Documentation
--------------------

Walkthrough the code to see some comments about design decisions. Along the files, methods and other structres you can find descriptions of decisions that were taken and the reasons supporting them.

The actual cartridge implementation should work straighforward on a clean SiteGenesis storefront. For other projects which have been customized, an advanced integration is required. 


----------


Thanks for reviewing. Detailed feedback is appreciated.

Known Issues:
- Unit testing is not provided because time limitations 

Spent time: 35 Hours.

INSTALLATION
------------

1) Add the cartridge to the Effective Cartridge Path:

- Go to Administration >  Sites >  Manage Sites > YOUR_SITE > Settings
- In the cartridges field, append the following ':int_bring'

Example:

sitegenesis_pipelines:sitegenesis_core:int_bring

2) Install the metadata definitions:

- Go to Administration >  Site Development >  Import & Export > Click on Upload
- Upload the metadata file that can be found in the distribution of this LINK Cartridge
- Go to Administration >  Site Development >  Import & Export > Clicn on Import 
- Select the recently uploaded file, and follow the Wizard. (Don't delete Existing attributes.)

3) Configure Custom Site Preferences:

- Go to Merchant Tools >  Site Preferences >  Custom Site Preferences > Bring
- If yoursite already includes the Google Maps Javascript file, uncheck the option "Include Google Maps Javascript File" 
- If yoursite does not use the Google Maps API, check the option, and configure the API Key. Not setting the API Key will result in the Javascript file not being included.
- Define the list of Countries suuported by Bring. (NO,DK,SE,FI)

4) Create the Service Definition:

A service named bring.pickup.bypostal needs to be created using the following data.

4a) Create the service using the following data:

Go to Administration >  Operations >  Services > click the New Button and 


    Name:bring.pickup.bypostal
    Type: HTTP	
    Enabled:	TRUE
    Service Mode:	Live
    Log Name Prefix: bring_pickup_bypostal
    Communication Log Enabled:	TRUE
    Profile:	simpleprofile
    Credentials:	bring.pickup.bypostal

4b) Create the profile

Go to Administration >  Operations >  Services > simpleprofile - Details, Click on the new button

    Name: simpleprofile
    Timeout (ms): 1,500
    Enable Circuit Breaker:	TRUE
    Max Circuit Breaker Calls:3

4c) Create the Credentials

Go to Administration >  Operations >  Services > Service Credentials, Click on the New button.

    Name: bring.pickup.bypostal
    URL: https://api.bring.com/pickuppoint/api/pickuppoint/{0}/postalCode/{1}.json

5) Configure Cache Settings: 

Make sure the Page Cache is enabled for your site. Go to Administration >  Sites >  Manage Sites > <YOUR SITE>- Cache

And check the  "Enable Page Caching" setting if you want the cache to store pickup locations retrieved from bring.
	
6) Add the bring modules definition to your module file:

- Open your core site's util/modules file and append the following statement:  

        <isinclude template="bring/modules"/>

Configuration
-------------

### Simple integration for storefronts based on SiteGenesis 15.8+:

1) Open pt_checkout_VARS.isml . At the end of the file add the folowing statement: 

    <isinclude template="util/modules" />  
    <isbringpickupbypostal />

2) Open <yoursite_cartridge>/cartridge/countries.json
	- Look for the 'dynamicForms' entry within the JSON object
	- Within 'dynamicForms' update the 'country' entry so that you add a "data-bring-country-field" attribute definition:   
		

    "country": {         
    			    "type": "select",    
    				"attributes": {    
    					"data-bring-country-field" : true    
    				}   
    			},    
    			
Within 'dynamicForms' update the "postal" entry so that you add a "data-bring-postal-field" attribute definition: 
 

    "postal":{
    				"attributes": {
    					"data-bring-postal-field" : true
    				}
    			},

3)	Go to your storefront, navigate to the checkout ( step 1), type in a postal code a map should appear, displaying the pickup locations near to the user's postal code.
 	
### Simple Integration for storefronts based on SiteGenesis prior to 15.8:  

1) Open pt_checkout_VARS.isml . At the end of the file add the folowing statement: 

    <isinclude template="util/modules" />  
    <isbringpickupbypostal />

2) Open your shipping form ISML template ( perhaps checkout/shipping/singleshipping.isml)
- update you postal code input tag to have data attributes "data-bring-postal-field"   

	<isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.zip}" atribute1="data-bring-postal-field" value1="true" />
- update you country code input tag to have data attributes "data-bring-country-field"
	
 

      <isinputfield formfield="${pdict.CurrentForms.singleshipping.shippingAddress.addressFields.country}"  atribute1="data-bring-country-field" type="select" />   	

3)	Go to your storefront, navigate to the checkout ( step 1), type in a postal code a map should appear, displaying the pickup locations near to the user's postal code.
 	
### ADVANCED INTEGRATION

1) Open pt_checkout_VARS.isml (or other decorator pt_xxxx_VARS.isml you want the functionality to be included) . At the end of the file add the folowing statement: 

 

      <isinclude template="util/modules" />  
       <isbringpickupbypostal p_init_template="/path/to/custom/template"/>

2) Make sure in your generated HTML markup there are 2 field. One flaged with the attibute data-bring-postal-field="true" and another flagged with the attribute data-bring-country-field="true"

3) Create the file /path/to/custom/template.isml
- copy the contents from int_bring/cartridge/templates/default/components/bring/pickupbypostalcode
- customize jQuery plugin initialization:

    function MyRenderPickUpLocationsLogic(){
    	//your logic here
    }
    
    function MyRenderPickupLocationsError(){
    	// your logic here
    }
    	
    jQuery(document).ready(function () {
    	jQuery(document).bringPickupPostal({
    		showNonSupportedCountryMessage: false,
    		renderPickUpLocationsCallback : MyRenderPickUpLocationsLogic, //rendering logic can be changed using this callback
    		renderPickupLocationsErrorCallback : MyRenderPickupLocationsError, // error rendering logic can be change using this callback
    		searchResultsCSSClass: "your-css-search-result"
    	});
    });


