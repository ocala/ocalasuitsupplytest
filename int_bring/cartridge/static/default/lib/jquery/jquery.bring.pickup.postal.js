/**
 * This plugin uses the window.Bring object, which is an object containing 
 * all the URLs, SitePrerences, and resource Labels used by the cartridge.
 * By using this object all the labels can be localized by means of DW resource files, so the plugin behavior
 * is fully localizable  
 */
(function($) {
	
	$.fn.bringPickupPostal = function(options) {
    	
		//Supported countries can be changed from the BUssines manager
    	var APISupportedCountries = Bring.SitePreferences.SUPPORTED_COUNTRIES;
    	var msg = null;
    	var nonSupportedCountryMsg = nonSupportedCountryMsgDefaults();
    	var throbber = null;
    	
    	var settings = $.extend({
    		showNonSupportedCountryMessage: true,
    		nonSupportedCountryMessageText : Bring.Resources.NON_SUPPORTED_COUNTRY_MESSAGE_TEXT + APISupportedCountries.join(","),
    		placeNonSupportedCountryMessage : nonSupportedCountryMsg.place,     //callback that defines a function that knows where place the error message
    		displayNonSupportedCountryMessage : nonSupportedCountryMsg.display, //callback that defines a function that knows how to display the error message
    		getNonSupportedCountryMessage : nonSupportedCountryMsg.get,         //callback that defines a function that knows how to get the error message
    		renderPickUpLocationsCallback : renderPickUpLocations, //rendering logic can be changed using this callback
    		renderPickupLocationsErrorCallback : renderPickupLocationsError, // error rendering logic can be change using this callback
    		showThrobberMethod : showSimpleBringThrobber,
    		hideThrobberMethod : hideSimpleBringThrobber,
    		prepareSearchResultWrapperCallback : prepareSearchResultWrapper,
    		searchResultsCSSClass: "bring-search-result",
    		nonSupportedCountryCSSClass : "bring-non-supported-country",
    		bringLoadingCSSClass : "bring-loading-throbber",
    		errorResultsCSSClass : "bring-error-result",
    		notFoundResultsCSSClass: "bring-no-results-found"
        }, options );
    	
    	///////// DEFAULT PLUGIN IMPLEMENTATION
    	/**
    	 * default strategy for handling the non supported country message
    	 * 
    	 * Known issue:
    	 * This logic should be extracted in another JS file, enhanced and 
    	 * re-implemented within the plugin so that all the layers and messages the plugin uses
    	 * can be handled through a similar strategy, (get, place, show)
    	 */
    	function nonSupportedCountryMsgDefaults(){
    		return {		
        		place : function(msg,postalCode){
        			var parent = postalCode.parent();
        			if(parent.find('.'+settings.nonSupportedCountryCSSClass).length <= 0){
        				parent.append(msg);
        			}
        		},
        		
        		display : function(msg){
        			msg.show();
        		},
        		
        		get : function(){
        			var nonSupportedCountryMessage = msg || $("<div/>").addClass(settings.nonSupportedCountryCSSClass).text(settings.nonSupportedCountryMessageText);
        	    	return nonSupportedCountryMessage;
        		}
        	}
    	}
    	
    	/**
    	 * Simple client side validation for country field. 
    	 * 
    	 */
    	function isCountrySupported(countryCode){
    		return APISupportedCountries.indexOf(countryCode.toUpperCase()) >= 0;
    	}
    	
    	/**
    	 * simple implemention for displaying the throbber
    	 */
    	function showSimpleBringThrobber(jqXHR,ajaxSettings,postalCode,country){
    		var th = throbber || $("<div/>").addClass(settings.bringLoadingCSSClass).text(Bring.Resources.LOADING_MSG);
    		postalCode.parent().append(th);
    		th.show();
    		postalCode.parent().parent().find("." + settings.searchResultsCSSClass).remove();
    		postalCode.parent().find("." + settings.errorResultsCSSClass).remove();
    		throbber = th;
    	}
    	
    	/**
    	 * Simple implementation for hiding the throbber
    	 */
    	function hideSimpleBringThrobber(responsedata,  textStatus, jqXHR ,postalCode,country){
    		if(throbber.hide){
    			throbber.hide();
    		}
    	} 
    	
    	
    	/**
    	 * Simple default implementation for a callback that allows 
    	 * preparing the container where search results will be displayed
    	 */
    	function prepareSearchResultWrapper(postalCode, appendClass){
    		//prepare the result displaying layer
    		postalCode.parent().parent().find("." + settings.searchResultsCSSClass).remove();
    		var result = $("<div/>").addClass(settings.searchResultsCSSClass);
    		if(appendClass){result.addClass(appendClass)}
    		postalCode.parent().parent().append(result);
    		return result;
    	}
    	
    	
    	/**
    	 * This method handles many error scenarios:
    	 * 
    	 * - postal code invalid format
    	 * - not supported country returned by bring (not client side)
    	 * - Demandware errors like socket timeout
    	 * - Other bring.com errors
    	 */
    	function renderPickupLocationsError(rejectValue){
    		//TODO... 
    		if(rejectValue.requestFields){
    			var postalCode = rejectValue.requestFields.postalCodeField;
        		postalCode.parent().find("." + settings.errorResultsCSSClass).remove();
        		var txt =null;
        		try{
        			var errorDetails = JSON.parse(rejectValue.errorMsg);
        			if(errorDetails.error){
        				var parts = errorDetails.error[0];
        				if(parts.parameter){
        					txt = Bring.Resources.ERROR_IN_PARAMETER + parts.parameter + ". " + parts.error;
        				}
        			}else{
        				throw new Error();
        			}
        			
        		}catch(e){
        			txt = rejectValue.errorCode + ":" + rejectValue.errorMsg;
        		}
        		var result = $("<div/>").addClass(settings.errorResultsCSSClass).text(txt);
        		postalCode.parent().append(result);
    		}else{
    			alert("Fatal Error:" + rejectValue);
    		}
    		
    	}
    	
    	//////////CORE PLUGIN CODE
    	/**
    	 * Decision 1: To find the input fields where the postal code and the country are defined
		 * the plugin will look for elements that have some data-xxx attributes defined.  
		 * 
		 * Argument 1: Using this approach the plugin will be always able to find the fields regardless 
		 * of where they are in the page. The process is unobtrusive , compliant with HTML5 and easy to implement.
		 * 
    	 * 
    	 */
    	return this.each( function() {
        	var elem = $(this);
        	var country = elem.find("[data-bring-country-field]").first();
        	var postalCode =  elem.find("[data-bring-postal-field]").first();
        	postalCode.on("change",function(){postalCodeChanged($(this),country)});
        }); 
    	
    	
    	 /**
    	 * - Decision 2: Since there is a limited set of supported countries by the Bring API a client side validation 
    	 * was implemented to avoid executing calls for unsupported countries. When the validation fails, a message could be displayed 
    	 * to notify the user. The message could be customized in content, place within the page and appearing logic using callbacks.
    	 *  
    	 * - Argument 2: Some sites could be localized but still displaying the complete entry list. In such 
    	 * cases users could accidentally change the country selected by default. Since country is most of
    	 * the times chosen after the postal code, and the functionality triggers onChange, the validation is helpful. 
    	 * Callbacks implemented in this way are a simple version of an strategy (getContent, placeContent, Show) pattern. 
    	 * 
    	 * - Decision 3: To use Promises for async task
    	 * - Argument 3: Promises are good for organizing async tasks, now most of the modern browsers support them so there are not 
    	 * reasons to avoid them.
    	 * 
    	 * - Decision 4: To allow for custom logic to render the results and the errors.
    	 * - Argument 4: Since the plugin should fit any project, customization is needed because rendering logic is always tied to the
    	 * layout and HTML markup which tends to change from one project to another. 
    	 */ 
    	function postalCodeChanged(postalCode,country){
        	var postaCodeValue = postalCode.val();
        	var countryValue = country.val();
        	if(isCountrySupported(countryValue)){
        		var getPickUpLocationPromise = new Promise(function(res,rej){getPickUpLocationExecutor(res,rej,postalCode,country);});        		
        		getPickUpLocationPromise.then(settings.renderPickUpLocationsCallback).catch(settings.renderPickupLocationsErrorCallback);
        	}else{
        		if(settings.showNonSupportedCountryMessage){
        			msg = settings.getNonSupportedCountryMessage(postalCode,country);
        			settings.placeNonSupportedCountryMessage(msg,postalCode,country);
        			settings.displayNonSupportedCountryMessage(msg,postalCode,country);
        		}    		
        	}    	
        }    
    	
    	/**
    	 * 
    	 * Decision 5: Implement a customizable throbber handlers.
    	 * Argument 5: By using a callbacks the developer will be able to show and hide the throbber in a custom way depending on the page layout
    	 * 
    	 * Decision 6: Promote Flexibility over information hidding in the callback implementations
    	 * Argument 6: Since this is a link cartridge aimed to fit any project, flexibility is promoted by passing all the available
    	 * information (parameters jqXHR,ajaxSettings which might not be needed) to the callbacks. This breaks information hidding principles 
    	 * but promotes flexibility because allows the developer to implement logic based on such info.
    	 * 
    	 */
    	function getPickUpLocationExecutor(resolve, reject,postalCode,country){
    		var requestData ={
    			'postalCode' : postalCode.val(),
    			'country'    : country.val()
    		};
    		var requestFields ={
    			'postalCodeField' : postalCode,
    			'countryField'    : country,
    		}
    		
    		$.ajax({
    			  url: Bring.Urls.pickUpByPostal,
    			  data: requestData,
    			  beforeSend: function(jqXHR,ajaxSettings){settings.showThrobberMethod(jqXHR,ajaxSettings,postalCode,country)},
    			  success: function( responsedata,  textStatus, jqXHR){
    				  responsedata = JSON.parse(responsedata.trim());
    				  if( !responsedata || ('error' in responsedata && responsedata.error)){
    					  responsedata = responsedata || {errorCode:-1,errorMsg:"Fatal Error"};
    					  responsedata.requestFields = requestFields;
    					  reject(responsedata);
    				  }
    				  var resolvedValue = { 'response': responsedata, 'request': requestFields};
    				  settings.hideThrobberMethod(responsedata,  textStatus, jqXHR ,postalCode,country );
    				  resolve(resolvedValue);
    			  }
    		});
    	}
    	
    	
    	/**
		 * Decision 7: To allow custom behavior for the NOT_FOUND_RESULT application state
		 * 
		 * 
		 * Known Issues: Even though renderPickUpLocations() is a custom behavior, 
		 * renderPickUpLocationsAsMap() should be also customizable. This allows developers to define a new way to render the map
		 * but still reusing the default prepareSearchResultWrapperCallback() implementation provided by this plugin
		 * 
		 */
		function renderPickUpLocations(resolvedValue){
    		var locations = resolvedValue.response.pickupPoint;
    		
    		if(locations.length == 0){
    			var postalCode = resolvedValue.request.postalCodeField;
        		result = settings.prepareSearchResultWrapperCallback(postalCode,settings.notFoundResultsCSSClass);
        		result.text(Bring.Resources.NO_RESULTS_FOUND);
    		}else{
    			//renderPickUpLocationsAsTable(resolvedValue.response,resolvedValue.request);
        		renderPickUpLocationsAsMap(resolvedValue.response,resolvedValue.request);
    		}
    		
    	}
		
		/**
		 * This is a very simple Google Maps implementation. 
		 * A more featured implementation should define a click listener on the markers
		 * To allow shipping address definition based on clicking pickup locations
		 * 
		 */
		function renderPickUpLocationsAsMap(pickUpLocations,requestData){
    		var locations = pickUpLocations.pickupPoint;
    		var postalCode = requestData.postalCodeField;
    		result = prepareSearchResultWrapper(postalCode);
    		
    		var myLatlng = new google.maps.LatLng(locations[0].latitude,locations[0].longitude);
    		var mapOptions = {
    		  zoom: 4,
    		  center: myLatlng
    		}
    		
    		var map = new google.maps.Map(result[0], mapOptions);
    		var bounds = new google.maps.LatLngBounds();
    		locations.forEach(function(item,index){
    			var _Latlng =  new google.maps.LatLng(item.latitude,item.longitude); 
    			var marker = new google.maps.Marker({
        		    position: _Latlng,
        		});
        		bounds.extend(marker.getPosition());
    			marker.setMap(map);    
    			
    			var contentString = "<h2>"+item.name+"</h2>"+"<h3>"+item.address+"</h3>";
    			var infowindow = new google.maps.InfoWindow({
    			    content: contentString
    			});
    			
    			marker.addListener('mouseover', function() {
    			    infowindow.open(map, marker);
    			});
    			
    			marker.addListener('mouseout', function() {
    			    infowindow.close();
    			});
    			
    		});
    		map.fitBounds(bounds);
    	}
    	
		
		/**
		 * This method was implemented as a first step in the test
		 * for bonus google maps was also implemented
		 * 
		 * @param pickUpLocations
		 * @param requestData
		 */
    	function renderPickUpLocationsAsTable(pickUpLocations,requestData){
    		var locations = pickUpLocations.pickupPoint;	
    		var postalCode = requestData.postalCodeField;
    		postalCode.parent().find("." + settings.searchResultsCSSClass).remove();
    		var result = $("<ol/>").addClass(settings.searchResultsCSSClass);
    		postalCode.parent().append(result);
    		locations.forEach(function(item,index){
    			result.append($("<li/>").html(item.name + " - " +  item.address));
    		});
    	}
		
    }
	
	/**
	 * Mockup method that was used for testing purposes. 
	 * Unit testing is not provided because time limitations.
	 * 
	 */
	function getMockNO_postal_1407(){
		return {
		    "pickupPoint": [
		                    {
		                        "id": "121524",
		                        "unitId": "121524",
		                        "name": "Coop Obs! Hypermarked Vinterbro",
		                        "address": "POSTBOKS 100",
		                        "postalCode": "1429",
		                        "city": "VINTERBRO",
		                        "countryCode": "NO",
		                        "municipality": "ÅS",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "SJØSKOGENVEIEN 7",
		                        "visitingPostalCode": "1407",
		                        "visitingCity": "VINTERBRO",
		                        "openingHoursNorwegian": "Man - Fre: 0800-2200, Lør: 0800-2000",
		                        "openingHoursEnglish": "Mon - Fri: 0800-2200, Sat: 0800-2000",
		                        "latitude": 59.73932700996702,
		                        "longitude": 10.770781349260394,
		                        "utmX": "262357",
		                        "utmY": "6629962",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=121524",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.739327009967,10.7707813492604",
		                        "distanceInKm": "1.3",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "4",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.73932701&destlong=10.77078135"
		                    },
		                    {
		                        "id": "102774",
		                        "unitId": "102774",
		                        "name": "Pakkeautomat Vinterbro Post i Butikk",
		                        "address": "SJØSKOGENVEIEN 7",
		                        "postalCode": "1407",
		                        "city": "VINTERBRO",
		                        "countryCode": "NO",
		                        "municipality": "ÅS",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "SJØSKOGENVEIEN 7",
		                        "visitingPostalCode": "1407",
		                        "visitingCity": "VINTERBRO",
		                        "locationDescription": "Pakkeautomaten er plassert i butikken i nærheten av postpunktet.",
		                        "openingHoursNorwegian": "Man - Fre: 0800-2200, Lør: 0800-2000",
		                        "openingHoursEnglish": "Mon - Fri: 0800-2200, Sat: 0800-2000",
		                        "latitude": 59.73953376857329,
		                        "longitude": 10.772732383961406,
		                        "utmX": "262468",
		                        "utmY": "6629978",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=102774",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.7395337685733,10.7727323839614",
		                        "distanceInKm": "1.4",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "27",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.73953377&destlong=10.77273238"
		                    },
		                    {
		                        "id": "121110",
		                        "unitId": "121110",
		                        "name": "Meny Greverud",
		                        "address": "POSTBOKS 14 GREVERUD",
		                        "postalCode": "1416",
		                        "city": "OPPEGÅRD",
		                        "countryCode": "NO",
		                        "municipality": "OPPEGÅRD",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "FLÅTESTADVEIEN 3",
		                        "visitingPostalCode": "1415",
		                        "visitingCity": "OPPEGÅRD",
		                        "locationDescription": "På Greverudsenteret",
		                        "openingHoursNorwegian": "Man - Fre: 0700-2200, Lør: 0900-2000",
		                        "openingHoursEnglish": "Mon - Fri: 0700-2200, Sat: 0900-2000",
		                        "latitude": 59.77397940554418,
		                        "longitude": 10.803229911496063,
		                        "utmX": "264423",
		                        "utmY": "6633700",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=121110",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.7739794055442,10.8032299114961",
		                        "distanceInKm": "8.6",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "4",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.77397941&destlong=10.80322991"
		                    },
		                    {
		                        "id": "176210",
		                        "unitId": "176210",
		                        "name": "Ski postkontor",
		                        "address": "POSTBOKS 333",
		                        "postalCode": "1401",
		                        "city": "SKI",
		                        "countryCode": "NO",
		                        "municipality": "SKI",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "ÅSENVEIEN 3",
		                        "visitingPostalCode": "1400",
		                        "visitingCity": "SKI",
		                        "locationDescription": "Ny inngang -Tidligere lokaler til Galleriet Møbler og Interiør",
		                        "openingHoursNorwegian": "Man - Fre: 0800-1800, Lør: 0900-1500",
		                        "openingHoursEnglish": "Mon - Fri: 0800-1800, Sat: 0900-1500",
		                        "latitude": 59.718080861872885,
		                        "longitude": 10.837977584435027,
		                        "utmX": "265981",
		                        "utmY": "6627360",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=176210",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.7180808618729,10.837977584435",
		                        "distanceInKm": "8.3",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "21",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.71808086&destlong=10.83797758"
		                    },
		                    {
		                        "id": "121390",
		                        "unitId": "121390",
		                        "name": "Rimi Sofiemyr",
		                        "address": "POSTBOKS 150",
		                        "postalCode": "1417",
		                        "city": "SOFIEMYR",
		                        "countryCode": "NO",
		                        "municipality": "OPPEGÅRD",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "SØNSTERUDVEIEN 26",
		                        "visitingPostalCode": "1412",
		                        "visitingCity": "SOFIEMYR",
		                        "openingHoursNorwegian": "Man - Fre: 0700-2300, Lør: 0800-2000",
		                        "openingHoursEnglish": "Mon - Fri: 0700-2300, Sat: 0800-2000",
		                        "latitude": 59.80339792444421,
		                        "longitude": 10.81332246739284,
		                        "utmX": "265196",
		                        "utmY": "6636936",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=121390",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.8033979244442,10.8133224673928",
		                        "distanceInKm": "12.2",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "4",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.80339792&destlong=10.81332247"
		                    },
		                    {
		                        "id": "121290",
		                        "unitId": "121290",
		                        "name": "Meny Langhus",
		                        "address": "POSTBOKS 150",
		                        "postalCode": "1403",
		                        "city": "LANGHUS",
		                        "countryCode": "NO",
		                        "municipality": "SKI",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "LANGHUSSENTERET 7",
		                        "visitingPostalCode": "1405",
		                        "visitingCity": "LANGHUS",
		                        "openingHoursNorwegian": "Man - Fre: 0800-2100, Lør: 0900-2000",
		                        "openingHoursEnglish": "Mon - Fri: 0800-2100, Sat: 0900-2000",
		                        "latitude": 59.75430892584159,
		                        "longitude": 10.844851627325168,
		                        "utmX": "266620",
		                        "utmY": "6631365",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=121290",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.7543089258416,10.8448516273252",
		                        "distanceInKm": "12.1",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "4",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.75430893&destlong=10.84485163"
		                    },
		                    {
		                        "id": "121488",
		                        "unitId": "121488",
		                        "name": "Coop Mega Kolbotn",
		                        "address": "POSTBOKS 1000 SENTRUM",
		                        "postalCode": "1418",
		                        "city": "KOLBOTN",
		                        "countryCode": "NO",
		                        "municipality": "OPPEGÅRD",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "KOLBOTNVEIEN 35",
		                        "visitingPostalCode": "1410",
		                        "visitingCity": "KOLBOTN",
		                        "openingHoursNorwegian": "Man - Fre: 0900-2100, Lør: 0900-1900",
		                        "openingHoursEnglish": "Mon - Fri: 0900-2100, Sat: 0900-1900",
		                        "latitude": 59.81039191043683,
		                        "longitude": 10.79807465075147,
		                        "utmX": "264391",
		                        "utmY": "6637768",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=121488",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.8103919104368,10.7980746507515",
		                        "distanceInKm": "13.9",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "4",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.81039191&destlong=10.79807465"
		                    },
		                    {
		                        "id": "102585",
		                        "unitId": "102585",
		                        "name": "Pakkeautomat Ås Post i Butikk",
		                        "address": "RAVEIEN 9",
		                        "postalCode": "1430",
		                        "city": "ÅS",
		                        "countryCode": "NO",
		                        "municipality": "ÅS",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "RAVEIEN 9",
		                        "visitingPostalCode": "1430",
		                        "visitingCity": "ÅS",
		                        "locationDescription": "Pakkeautomaten er plassert i butikken i nærheten av kassene.",
		                        "openingHoursNorwegian": "Man - Lør: 0700-2300",
		                        "openingHoursEnglish": "Mon - Sat: 0700-2300",
		                        "latitude": 59.665473989118425,
		                        "longitude": 10.792151933955502,
		                        "utmX": "263035",
		                        "utmY": "6621672",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=102585",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.6654739891184,10.7921519339555",
		                        "distanceInKm": "12.1",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "27",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.66547399&destlong=10.79215193"
		                    },
		                    {
		                        "id": "121447",
		                        "unitId": "121447",
		                        "name": "Coop Extra Raveien",
		                        "address": "POSTBOKS 230",
		                        "postalCode": "1431",
		                        "city": "ÅS",
		                        "countryCode": "NO",
		                        "municipality": "ÅS",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "RAVEIEN 9",
		                        "visitingPostalCode": "1430",
		                        "visitingCity": "ÅS",
		                        "openingHoursNorwegian": "Man - Lør: 0700-2300",
		                        "openingHoursEnglish": "Mon - Sat: 0700-2300",
		                        "latitude": 59.66537306004099,
		                        "longitude": 10.791809144246493,
		                        "utmX": "263015",
		                        "utmY": "6621662",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=121447",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.665373060041,10.7918091442465",
		                        "distanceInKm": "12.3",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "4",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.66537306&destlong=10.79180914"
		                    },
		                    {
		                        "id": "121355",
		                        "unitId": "121355",
		                        "name": "Kiwi Fagerstrand",
		                        "address": "POSTBOKS 37",
		                        "postalCode": "1457",
		                        "city": "FAGERSTRAND",
		                        "countryCode": "NO",
		                        "municipality": "FROGN",
		                        "county": "AKERSHUS",
		                        "visitingAddress": "MYKLERUDVEIEN 65",
		                        "visitingPostalCode": "1454",
		                        "visitingCity": "FAGERSTRAND",
		                        "openingHoursNorwegian": "Man - Lør: 0700-2300",
		                        "openingHoursEnglish": "Mon - Sat: 0700-2300",
		                        "latitude": 59.74220159712348,
		                        "longitude": 10.603063676303183,
		                        "utmX": "252963",
		                        "utmY": "6630895",
		                        "postenMapsLink": "http://www.posten.no/kundeservice/kart?ID=121355",
		                        "googleMapsLink": "http://maps.google.com/maps?f=q&hl=en&geocode=&q=59.7422015971235,10.6030636763032",
		                        "distanceInKm": "16.0",
		                        "distanceType": "DRIVING_DISTANCE",
		                        "type": "4",
		                        "additionalServiceCode": "1036",
		                        "routeMapsLink": "https://api.bring.com/pickuppoint/map/route?origlat=59.734866&origlong=10.75588&destlat=59.7422016&destlong=10.60306368"
		                    }
		                ]
		            };
	}
 
}(jQuery));